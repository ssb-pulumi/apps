import { IApp } from "../types";

export class Angel implements IApp {
  readonly name: string;
  readonly exposed: boolean;
  readonly replicas: number;
  readonly image: string;
  readonly containerPort: number;
  readonly serviceType: "ClusterIP" | "LoadBalancer";

  constructor(tag: string) {
    this.name = "angel";
    this.exposed = true;
    this.replicas = 1;
    this.image = tag;
    this.containerPort = 80;
    this.serviceType = "ClusterIP" as "ClusterIP" | "LoadBalancer";
  }
}