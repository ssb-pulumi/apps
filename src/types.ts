import { Config, StackReference } from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

const config = new Config();
const infrastructure = new StackReference(
  config.require("infrastructure")
);

export const DOMAIN_NAME = infrastructure.getOutput("domainName");
export const KUBE_CONFIG = infrastructure.getOutput("kubeConfig");


export interface IApp {
  name: string;
  exposed: boolean;
  replicas: number;
  image: string;
  containerPort: number;
  serviceType: "ClusterIP" | "LoadBalancer";
  pod?: k8s.core.v1.Pod;
  deploymentStrategy?: "Recreate" | "RollingUpdate";
  configMap?: k8s.core.v1.ConfigMap;
}
