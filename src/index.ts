import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import { Config, StackReference, Output } from "@pulumi/pulumi";
import { KUBE_CONFIG } from "./types";
import { App } from "../src/lib/app";
import { Aero } from "../src/k8s-apps/aero";
import { Angel } from "../src/k8s-apps/angel";

const config = new Config();
const aeroImageTag = config.require("aero-image");
const angelImageTag = config.require("angel-image");

const providerRenderYaml = (app: string) =>
  new k8s.Provider("render-yaml", {
    renderYamlToDirectory: `${pulumi.getStack()}/${app}`,
  });

const provider = new k8s.Provider("k8s", { kubeconfig: KUBE_CONFIG });

// Kubernetes App(Rendered YAML and deployed through Arocd)
new App(new Aero(aeroImageTag), providerRenderYaml("aero"));

// Kubernetes App deployed using pulumi
new App(new Angel(angelImageTag), provider);
