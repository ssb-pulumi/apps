import { IApp } from "../types";
import * as k8s from "@pulumi/kubernetes";
import { DOMAIN_NAME } from "../types";

export class App {
  constructor(appProps: IApp, provider: k8s.Provider) {
    const appName = appProps.name;
    const appLabels = { app: appProps.name };

    const namespace = new k8s.core.v1.Namespace(
      appName,
      {
        metadata: {
          annotations: {
            "argocd.argoproj.io/instance": appProps.name,
          },
          labels: {
            "app.kubernetes.io/instance": appProps.name,
            app: appProps.name,
          },
          name: appName
        },
      },
      { provider }
    );

    const deployment = new k8s.apps.v1.Deployment(
      appName,
      {
        metadata: {
          namespace: namespace.metadata.name,
          annotations: {
            "argocd.argoproj.io/instance": appProps.name,
          },
          labels: {
            "app.kubernetes.io/instance": appProps.name,
            app: appProps.name,
          },
        },
        spec: {
          selector: { matchLabels: appLabels },
          replicas: appProps.replicas,
          template: {
            metadata: { labels: appLabels },
            spec: { containers: [{ name: appName, image: appProps.image }] },
          },
        },
      },
      { provider }
    );

    const service = new k8s.core.v1.Service(
      appName,
      {
        metadata: {
          labels: {
            "app.kubernetes.io/instance": appProps.name,
            app: appProps.name,
          },
          namespace: namespace.metadata.name,
          annotations: { "argocd.argoproj.io/instance": appProps.name },
        },
        spec: {
          type:
            appProps.serviceType === "ClusterIP" ? "ClusterIP" : "LoadBalancer",
          ports: [
            { port: 80, targetPort: appProps.containerPort, protocol: "TCP" },
          ],
          selector: appLabels,
        },
      },
      { provider }
    );

    const ingressngress = DOMAIN_NAME.apply((dn) => {
      new k8s.networking.v1beta1.Ingress(
        appName,
        {
          metadata: {
            namespace: namespace.metadata.name,
            labels: {
              app: appName,
              "app.kubernetes.io/instance": appProps.name,
            },
            annotations: {
              "kubernetes.io/ingress.class": "nginx",
              "argocd.argoproj.io/instance": appProps.name,
            },
          },
          spec: {
            rules: [
              {
                host: `${appName}.${dn}`,
                http: {
                  paths: [
                    {
                      path: "/",
                      backend: {
                        serviceName: service.metadata.name,
                        servicePort: 80,
                      },
                    },
                  ],
                },
              },
            ],
          },
        },
        { provider }
      );
    });
  }
}
